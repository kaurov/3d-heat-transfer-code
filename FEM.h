//Data structures and solvers
#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/function.h>
#include <deal.II/base/logstream.h>
#include <deal.II/base/tensor_function.h>
#include <deal.II/lac/vector.h>
#include <deal.II/lac/full_matrix.h>
#include <deal.II/lac/sparse_matrix.h>
#include <deal.II/lac/sparse_direct.h>
#include <deal.II/numerics/vector_tools.h>
#include <deal.II/numerics/matrix_tools.h>
#include <deal.II/numerics/data_out.h>
//Mesh related classes
#include <deal.II/grid/tria.h>
#include <deal.II/grid/grid_generator.h>
#include <deal.II/grid/tria_accessor.h>
#include <deal.II/grid/tria_iterator.h>
#include <deal.II/grid/tria_boundary_lib.h>
#include <deal.II/grid/grid_tools.h>
#include <deal.II/dofs/dof_handler.h>
#include <deal.II/dofs/dof_accessor.h>
#include <deal.II/dofs/dof_tools.h>
//Finite element implementation classes
#include <deal.II/fe/fe_system.h>
#include <deal.II/fe/fe_values.h>
#include <deal.II/fe/fe_q.h>
//Standard C++ libraries
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>

#include <deal.II/grid/manifold_lib.h>
using namespace dealii;

const unsigned int order = 1;
const unsigned int quadRule = 2;

#define r(i) sqrt(pow(nodeLocation[i][0],2)+pow(nodeLocation[i][1],2)+pow(nodeLocation[i][2],2))

template <int dim>
class FEM
{
public:

    FEM (double Alpha);
    ~FEM();

    void generate_mesh();
    void define_boundary_conds();
    void setup_system();
    void assemble_system();
    void apply_initial_conditions();
    void solve_trans();
    double Temperature(double r, double t);
    void output_dat_file();

    void output_trans_results(unsigned int index);

    double C(double T);
    double kappa(double T);
    double Q(double T);

    double R = 1.e6;
    double pi = 3.141592653589793238;                         // number pi
    double yrtosec = 31536000;                                // from years to seconds factor

    double l2norm();

    //Class objects
    Triangulation<dim> triangulation;                         //mesh
    FESystem<dim>      fe;                                    //FE element
    DoFHandler<dim>    dof_handler;                           // Connectivity matrices

    QGauss<dim>   quadrature_formula;                         //Quadrature
    QGauss<dim-1> face_quadrature_formula;                    //Face Quadrature

    //Data structures
    SparsityPattern      sparsity_pattern;                    //Sparse matrix pattern
    SparseMatrix<double> M, K, system_matrix;                 //Global stiffness matrix - Sparse matrix - used in the solver
    Vector<double>       D_steady, D_trans, V_trans, F, RHS;  //Global vectors - Solution vector (D) and Global force vector (F)

    Table<2,double>	              nodeLocation;	              //Table of the coordinates of nodes by global dof number
    std::map<unsigned int,double> boundary_values_of_D;       //Map of dirichlet boundary conditions for the temperature
    std::map<unsigned int,double> boundary_values_of_V;       //Map of dirichlet boundary conditions for the time derivative of temperature

    std::vector<double> l2norm_results;                       //A vector to store the l2norms calculated in the time loop in solve_trans()
    double	            alpha; 	                              //Specifies the Euler method, 0 <= alpha <= 1


    //solution name array
    std::vector<std::string> nodal_solution_names;
    std::vector<DataComponentInterpretation::DataComponentInterpretation> nodal_data_component_interpretation;
};

// Class constructor for a scalar field
template <int dim>
FEM<dim>::FEM (double Alpha)
        :
        fe (FE_Q<dim>(order), 1),
        dof_handler (triangulation),
        quadrature_formula(quadRule),
        face_quadrature_formula(quadRule)
{
    alpha = Alpha;

    nodal_solution_names.push_back("D");
    nodal_data_component_interpretation.push_back(DataComponentInterpretation::component_is_scalar);
}

//Class destructor
template <int dim>
FEM<dim>::~FEM (){dof_handler.clear ();}

//Define the problem domain and generate the mesh
template <int dim>
void FEM<dim>::generate_mesh(){

    double  x_center = 0., //EDIT
            y_center = 0., //EDIT
            z_center = 0.;//EDIT

    Point<dim,double> center(x_center,y_center,z_center);
    GridGenerator::hyper_ball (triangulation, center, R*0.99);


    static const HyperBallBoundary<dim> sphere(center,R*0.99);
    triangulation.set_boundary (0, sphere);

    triangulation.refine_global (3);

}

template <int dim>
double FEM<dim>::C(double T)
{
    double C_0 = 1.e12; //12
    return C_0*T;
}

template <int dim>
double FEM<dim>::kappa(double T)
{
    double kappa_0 = 1.e12; //12
    return kappa_0*T;
}

template <int dim>
double FEM<dim>::Q(double T)
{
    double Q_0 = 1.e1;
    return Q_0*T*T;
}

template <int dim>
double FEM<dim>::Temperature(double r, double t)                      // Analytical solution
{
    double T_0 = 1e10;
    double k = pi/(R) ;                                               // minimal wavenumber of a thermal wave
    double C_0 = 1.e12     ;                                          // heat capacity coefficient
    double kappa_0 = 1.e12  ;                                         // thermal conductivity coefficient
    double Q_0 = 10. ;                                                // eutrino emissivity coefficient
    double gamma = (0.5 * kappa_0 * k * k + Q_0)/C_0;                 // coefficient which is responsible for T(r) dependence
    return T_0 * exp(-gamma * t) * sqrt(sin(k * r)/(k * r));;
}


//Specify the Dirichlet boundary conditions
template <int dim>
void FEM<dim>::define_boundary_conds(){

    /*
    const unsigned int totalNodes = dof_handler.n_dofs(); //Total number of nodes

    for(unsigned int globalNode=0; globalNode<totalNodes; globalNode++){
        if(sqrt(pow(nodeLocation[globalNode][0],2) + pow(nodeLocation[globalNode][1],2) + pow(nodeLocation[globalNode][2],2)) >= 0.99999){
            boundary_values_of_D[globalNode] = 0.;
            boundary_values_of_V[globalNode] = 0.;
        }
        else if(sqrt(pow(nodeLocation[globalNode][0],2) + pow(nodeLocation[globalNode][1],2) + pow(nodeLocation[globalNode][2],2)) <= 0.1){
            boundary_values_of_D[globalNode] = 300.;
            boundary_values_of_V[globalNode] = 0.;
        }
    }
    */
}

//Setup data structures (sparse matrix, vectors)
template <int dim>
void FEM<dim>::setup_system(){

    //Let deal.II organize degrees of freedom
    dof_handler.distribute_dofs (fe);

    //Fill in the Table "nodeLocations" with the x, y, and z coordinates of each node by its global index
    MappingQ1<dim,dim> mapping;
    std::vector< Point<dim,double> > dof_coords(dof_handler.n_dofs());
    nodeLocation.reinit(dof_handler.n_dofs(),dim);
    DoFTools::map_dofs_to_support_points<dim,dim>(mapping,dof_handler,dof_coords);
    for(unsigned int i=0; i<dof_coords.size(); i++){
        for(unsigned int j=0; j<dim; j++){
            nodeLocation[i][j] = dof_coords[i][j];
        }
    }

    //Specify boundary condtions (call the function)
    define_boundary_conds();

    //Define the size of the global matrices and vectors
    sparsity_pattern.reinit (dof_handler.n_dofs(),
                             dof_handler.n_dofs(),
                             dof_handler.max_couplings_between_dofs());
    DoFTools::make_sparsity_pattern (dof_handler, sparsity_pattern);
    sparsity_pattern.compress();
    K.reinit (sparsity_pattern);
    M.reinit (sparsity_pattern);
    system_matrix.reinit (sparsity_pattern);
    D_steady.reinit(dof_handler.n_dofs());
    D_trans.reinit(dof_handler.n_dofs());
    V_trans.reinit(dof_handler.n_dofs());
    RHS.reinit(dof_handler.n_dofs());
    F.reinit(dof_handler.n_dofs());

    //Just some notes...
    std::cout << "   Number of active elems:       " << triangulation.n_active_cells() << std::endl;
    std::cout << "   Number of degrees of freedom: " << dof_handler.n_dofs() << std::endl;
}

//Form elmental vectors and matrices and assemble to the global vector (F) and matrix (K)
template <int dim>
void FEM<dim>::assemble_system(){

    M=0; K=0; F=0;

    FEValues<dim> fe_values(fe,
                            quadrature_formula,
                            update_values |
                            update_gradients |
                            update_JxW_values);

    FEFaceValues<dim> fe_face_values (fe,
                                      face_quadrature_formula,
                                      update_values |
                                      update_quadrature_points |
                                      update_JxW_values);

    const unsigned int num_face_quad_pts = face_quadrature_formula.size();    //Total number of quad points in the face
    const unsigned int faces_per_elem = GeometryInfo<dim>::faces_per_cell;

    const unsigned int dofs_per_elem = fe.dofs_per_cell;                      //This gives you dofs per element
    unsigned int 	   num_quad_pts = quadrature_formula.size();              //Total number of quad points in the element

    FullMatrix<double> Mlocal (dofs_per_elem, dofs_per_elem);
    FullMatrix<double> Klocal (dofs_per_elem, dofs_per_elem);
    Vector<double>     Flocal (dofs_per_elem);

    std::vector<unsigned int> local_dof_indices (dofs_per_elem);              //This relates local dof numbering to global dof numbering

    typename DoFHandler<dim>::active_cell_iterator elem = dof_handler.begin_active (), endc = dof_handler.end();
    for (;elem!=endc; ++elem){

        elem->get_dof_indices (local_dof_indices);
        fe_values.reinit(elem);
        elem->get_dof_indices (local_dof_indices);

        Mlocal = 0.;
        for(unsigned int q=0; q<num_quad_pts; q++){
            double D_trans_at_q = 0.;
            for(unsigned int C=0; C<dofs_per_elem; C++){
                D_trans_at_q += D_trans[local_dof_indices[C]]*fe_values.shape_value(C,q);
            }
            for(unsigned int A=0; A<fe.dofs_per_cell; A++){
                for(unsigned int B=0; B<fe.dofs_per_cell; B++){
                    Mlocal[A][B] += fe_values.shape_value(A,q)*fe_values.shape_value(B,q)*C(D_trans_at_q)*fe_values.JxW(q);
                }
            }
        }

        Klocal = 0.;
        for(unsigned int A=0; A<fe.dofs_per_cell; A++){
            for(unsigned int B=0; B<fe.dofs_per_cell; B++){
                for(unsigned int q=0; q<num_quad_pts; q++){
                    double D_trans_at_q = 0.;
                    for(unsigned int C=0; C<dofs_per_elem; C++){
                        D_trans_at_q += D_trans[local_dof_indices[C]]*fe_values.shape_value(C,q);
                    }
                    for(unsigned int i=0; i<dim; i++){
                        for(unsigned int j=0; j<dim; j++){
                            if(i==j){
                                Klocal[A][B] += fe_values.shape_grad(A,q)[i]*kappa(D_trans_at_q)*fe_values.shape_grad(B,q)[j]*fe_values.JxW(q);
                            }
                        }
                    }
                }
            }
        }


        Flocal = 0.;
        for(unsigned int A=0; A<dofs_per_elem; A++){
            for(unsigned int q=0; q<num_quad_pts; q++) {
                double D_trans_at_q = 0.;
                for(unsigned int C=0; C<dofs_per_elem; C++){
                    D_trans_at_q += D_trans[local_dof_indices[C]]*fe_values.shape_value(C,q);
                }
                Flocal[A] -= fe_values.shape_value(A,q)*Q(D_trans_at_q)*fe_values.JxW(q);
            }
        }

        for (unsigned int f=0; f < faces_per_elem; f++){
            fe_face_values.reinit (elem, f);
            if(sqrt(pow(elem->face(f)->center()[0],2)+pow(elem->face(f)->center()[1],2)+pow(elem->face(f)->center()[2],2)) >= R*0.999){
                for (unsigned int q=0; q<num_face_quad_pts; ++q){
                    for (unsigned int A=0; A<dofs_per_elem; A++){
                        //Flocal[A] -= 100*fe_face_values.shape_value(A,q)*fe_face_values.JxW(q);
                        Flocal[A] = 0.;
                    }
                }
            }
        }


        for (unsigned int i=0; i<dofs_per_elem; ++i){
            F[local_dof_indices[i]] += Flocal[i];
            for (unsigned int j=0; j<dofs_per_elem; ++j){
                K.add(local_dof_indices[i],local_dof_indices[j],Klocal[i][j]);
                M.add(local_dof_indices[i],local_dof_indices[j],Mlocal[i][j]);
            }
        }
    }
}

//Apply initial conditions for the transient problem
template <int dim>
void FEM<dim>::apply_initial_conditions(){

    const unsigned int totalNodes = dof_handler.n_dofs(); //Total number of nodes

    for(unsigned int i=0; i<totalNodes; i++){
        D_trans[i] = Temperature(r(i),0.);
    }

}

template <int dim>
void FEM<dim>::solve_trans(){

    const double delta_t = 1e9;
    const unsigned int totalNodes = dof_handler.n_dofs();
    Vector<double>     D_tilde(totalNodes);

    {
        apply_initial_conditions();
        assemble_system();

        //Find V_0 = M^{-1}*(F_0 - K*D_0)
        system_matrix.copy_from(M); //system_matrix = M

        //We need to define the right-hand-side vector (RHS = F_0 - K*D_0) step by step...
        K.vmult(RHS,D_trans); //RHS = K*D_trans
        RHS *= -1.; 		//RHS = -1.*RHS = -K*D_trans
        RHS.add(1.,F); 	//RHS = RHS + 1.*F = F - K*D_trans
        MatrixTools::apply_boundary_values (boundary_values_of_V, system_matrix, V_trans, RHS, false);

        //Solve for solution in system_matrix*V_trans = RHS
        SparseDirectUMFPACK  A;
        A.initialize(system_matrix);
        A.vmult (V_trans, RHS); //V_trans=system_matrix^{-1}*RHS

        //Output initial state
        output_trans_results(0);

        //double current_l2norm = l2norm();
        //l2norm_results.push_back(current_l2norm);
    }

    //Loop over time steps. For each time step, update D_transient from D_n to D_{n+1} using the V method
    unsigned int counter = 0;

    for(double t_step=delta_t; t_step<2e10+1; t_step += delta_t){

        counter ++;

        assemble_system();
        
        /*SparseMatrix and Vector operations in deal.II:
          To add a matrix with a coefficient to system matrix, use system_matrix.add(coeffient,matrix)
          To add a vector with a coefficient to RHS, use RHS.add(coefficient,vector)
          To multiply a sparse matrix and a vector, for example RHS = K*D_tilde, use
          K.vmult(RHS,D_tilde)
          To set system_matrix equal to M, use system_matrix.copy_from(M)
          To set RHS equal to F, use RHS = F
          To set RHS equal to -RHS, use RHS *= -1
          For some examples, look at the apply_initial_conditions() function*/

        //Find D_tilde. Remember, at this point D_trans = D_n and V_trans = V_n

        std::cout << "time = " << t_step << " sec" << std::endl;

        D_tilde = D_trans;
        D_tilde.add(delta_t*(1-alpha),V_trans);
        system_matrix.copy_from(M);
        system_matrix.add(alpha*delta_t,K);

        K.vmult(RHS,D_tilde);
        RHS *= -1.;
        RHS.add(1.,F);

        /*Use D_tilde to update V_trans from V_n to V_{n+1}. This involves solving
          a matrix/vector system: system_matrix*Vtrans = RHS. You need to define
          system_matrix and RHS to correctly solve for V_trans = V_{n+1} = system_matrix^{-1}*RHS*/

        //Apply boundary conditions on V_trans before solving the matrix/vector system
        MatrixTools::apply_boundary_values (boundary_values_of_V, system_matrix, V_trans, RHS, false);

        //Solve for V_trans (V_{n+1}) in system_matrix*solution = RHS
        SparseDirectUMFPACK  A;
        A.initialize(system_matrix);
        A.vmult (V_trans, RHS); //V_trans=system_matrix^{-1}*RHS

        /*To clarify, .vmult with a SparseDirectUMFPACK matrix (like A)
          multiplies a vector by the matrix inverse,
          but .vmult with a SparseMatrix (like K or M)
          multiplies a vector by the matrix itself (non-inverted)*/

        //Update D_trans to D_{n+1} using D_tilde and V_trans (V_{n+1})

        D_trans = D_tilde;
        D_trans.add(alpha*delta_t,V_trans);


        output_trans_results(counter);

        //    double current_l2norm = l2norm();
        //   l2norm_results.push_back(current_l2norm);

    }
    output_dat_file();
}

//Output transient results for a given time step
template <int dim>
void FEM<dim>::output_trans_results (unsigned int index){
    //This adds an index to your filename so that you can distinguish between time steps

    //Write results to VTK file
    char filename[100];
    snprintf(filename, 100, "./output/solution_%d.vtk", index);
    std::ofstream output1 (filename);
    DataOut<dim> data_out; data_out.attach_dof_handler (dof_handler);

    //Add nodal DOF data
    data_out.add_data_vector (D_trans, nodal_solution_names, DataOut<dim>::type_dof_data, nodal_data_component_interpretation);
    data_out.build_patches (); data_out.write_vtk (output1); output1.close();
}

//Function to calculate the l2norm of the difference between the current and steady state solutions.
template <int dim>
double FEM<dim>::l2norm(){
    double l2norm = 0.;

    FEValues<dim> fe_values(fe,
                            quadrature_formula,
                            update_values |
                            update_JxW_values);

    const unsigned int 				dofs_per_elem = fe.dofs_per_cell; //This gives you dofs per element
    std::vector<unsigned int> local_dof_indices (dofs_per_elem);
    const unsigned int 				num_quad_pts = quadrature_formula.size(); //Total number of quad points in the element
    double 										u_steady, u_trans;

    //loop over elements
    typename DoFHandler<dim>::active_cell_iterator elem = dof_handler.begin_active (),
            endc = dof_handler.end();
    for (;elem!=endc; ++elem){
        elem->get_dof_indices (local_dof_indices);
        fe_values.reinit(elem);

        for(unsigned int q=0; q<num_quad_pts; q++){
            u_steady = 0.; u_trans = 0.;
            for(unsigned int A=0; A<dofs_per_elem; A++){
                /*//EDIT - interpolate the steady state solution (u_steady) and transient solution (u_trans)
                  at the current quadrature point using D_steady and D_trans. Similar to finding u_h in HW2*/
                u_steady += D_steady[local_dof_indices[A]]*fe_values.shape_value(A,q);
                u_trans += D_trans[local_dof_indices[A]]*fe_values.shape_value(A,q);
            }
            //EDIT - define the l2norm of the difference between u_steady and u_trans
            l2norm += pow((u_steady - u_trans),2)*fe_values.JxW(q);
        }

    }

    //The l2norm is the square root of the integral of (u_steady - u_trans)^2, using D_steady and D_trans

    return sqrt(l2norm);
}

template <int dim>
void FEM<dim>::output_dat_file(){

    const unsigned int totalNodes = dof_handler.n_dofs(); //Total number of nodes

    FILE *coolingdata;
    coolingdata = std::fopen("coolingdata.dat", "w" );

    for(unsigned  int i=0; i<totalNodes; i++)
        std::fprintf(coolingdata, "%14.7e  %14.7e\n", r(i), D_trans[i]);

    std::fclose(coolingdata);


}